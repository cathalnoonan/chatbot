﻿using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QnABot.MessageHandlers.Contracts
{
    public interface IMessageResponseHandler
    {
        /// <summary>
        ///     Returns true if the handler supports the requested topic.
        /// </summary>
        /// <param name="messageText">
        ///     The message sent by the user.
        /// </param>
        /// <returns>
        ///     True if the handler can support the requested topic.
        /// </returns>
        bool SupportsTopic(string messageText);

        /// <summary>
        ///     Supported topics needs to be provided for the the <see cref="IMessageReceivedHandler"/>
        ///     to decide which <see cref="IMessageResponseHandler"/> to use.
        /// </summary>
        List<string> SupportedTopics { get; set; }


        /// <summary>
        ///     Returns a response to the user based on the value provided for <paramref name="messageText"/>.
        /// </summary>
        /// <param name="messageText">
        ///     The message received from the user.
        /// </param>
        /// <returns>
        ///     The response message.
        /// </returns>
        Activity CreateResponse(Activity activity, string messageText);
    }
}
