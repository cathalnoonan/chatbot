﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.CognitiveServices.QnAMaker;
using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using System.Configuration;
using System.Collections.Generic;
using QnABot.Dto;
using QnABot.Dialogs;
using QnABot.MessageHandlers.Contracts;

namespace QnABot.Dialogs
{
    public class RootDialog2 : IDialog<object>
    {
        protected const string QNA_AUTH_KEY_CONFIGURATION_KEY_NAME = "QnAAuthKey";
        protected const string QNA_KNOWLEDGE_BASE_CONFIGURATION_KEY_NAME = "QnAKnowledgebaseId";
        protected const string QNA_ENDPOINT_HOST_NAME_CONFIGURATION_KEY_NAME = "QnAEndpointHostName";
        protected const string TRANSFER_MESSAGE = "transfer to";

        private static List<string> suggestedEscallations = new List<string>();

        public async Task StartAsync(IDialogContext context)
        {
            /* Wait until the first message is received from the conversation and call MessageReceviedAsync 
            *  to process that message. */
            context.Wait(this.MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            /* When MessageReceivedAsync is called, it's passed an IAwaitable<IMessageActivity>. To get the message,
             *  await the result. */
            var message = await result;

            string userId = context.ConversationData.GetValue<string>("id");

            // 1  Check if user was suggested an escalation AND message.Text = yes:
            //      1.1  Remove the user from list of users suggested escalations.
            //      1.2  Escalate chat to Dynamics user (aka. TRANSFER_TO).
            // 2  Handle Else:
            //      2.1  If exists, remove the user from the suggested escalations.
            //      2.2  Check if QNA can handle the question, return answer.
            //      2.3  Otherwise, check if KB can handle the question, return answer.
            //      2.4  Otherwise, suggest an escalation to a Dynamics user.

            Activity response = (Activity) context.MakeMessage();
            bool suggestedEscallationsContainsUserId = suggestedEscallations.Contains(userId);
            ConnectorClient connector = new ConnectorClient(new Uri(response.ServiceUrl));

            if (suggestedEscallationsContainsUserId && IsMessageYes(message.Text))
            {
                suggestedEscallations.Remove(userId);

                //IEscalateToDynamicsUserHandler handler = null;            // todo
                //handler.GetResponse(response, message.Text);

                EscalateToDynamicsUser(message.Text);
            }
            else
            {
                if (suggestedEscallationsContainsUserId)
                {
                    suggestedEscallations.Remove(userId);
                }

                // Declare handlers.
                IQuestionAndAnswerHandler questionAndAnswerHandler = null;  // todo
                IKnowledgeBaseHandler knowledgeBaseHandler = null;          // todo
                ISuggestEscalationHandler suggestEscalationHandler = null;  // todo

                // Find the correct handler for the message.
                IMessageResponseHandler handler =
                    questionAndAnswerHandler.SupportsTopic(message.Text) ? questionAndAnswerHandler
                    : knowledgeBaseHandler.SupportsTopic(message.Text) ? knowledgeBaseHandler
                    : (IMessageResponseHandler) suggestEscalationHandler;

                response = handler.CreateResponse(response, message.Text);
            }

            await connector.Conversations.SendToConversationAsync(response);
        }

        private void EscalateToDynamicsUser(string text)
        {
            throw new NotImplementedException();
        }

        private bool IsMessageYes(string text)
        {
            bool result = false;
            text = text.Trim().ToLower();
            switch (text)
            {
                case "y":
                case "yes":
                case "please":
                    result = true;
                    break;
            }
            return result;
        }

        private async Task AfterAnswerAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            // wait for the next user message
            context.Wait(MessageReceivedAsync);
        }


        [Obsolete("Use ConfigurationManager.AppSettings[<<key>>] instead", true)]
        private static string GetSetting(string key)
        {
            var value = Utils.GetAppSetting(key);
            if (String.IsNullOrEmpty(value) && key == "QnAAuthKey")
            {
                value = Utils.GetAppSetting("QnASubscriptionKey"); // QnASubscriptionKey for backward compatibility with QnAMaker (Preview)
            }
            return value;
        }

        private static string GenerateGoogleSearchUrl(string messageText)
        {
            // Escape the query.
            string httpEscapedText = System.Security.SecurityElement.Escape(messageText);

            // Replace the %20 with a space so we can use String.Split().
            string searchParam = string.Join("+", httpEscapedText.Replace("%20", " ").Split(' '));

            string googleSearch = $"I don't know.. Let me google that for you!\nhttps://www.google.com/search?q={ searchParam }";

            return messageText;
        }
    }
}