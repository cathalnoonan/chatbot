using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.CognitiveServices.QnAMaker;
using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using System.Configuration;
using System.Collections.Generic;
using QnABot.Dto;
using QnABot.Dialogs;

namespace Microsoft.Bot.Sample.QnABot
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        internal const string QNA_AUTH_KEY_CONFIGURATION_KEY_NAME = "QnAAuthKey";
        internal const string QNA_KNOWLEDGE_BASE_CONFIGURATION_KEY_NAME = "QnAKnowledgebaseId";
        internal const string QNA_ENDPOINT_HOST_NAME_CONFIGURATION_KEY_NAME = "QnAEndpointHostName";
        public const string TRANSFER_MESSAGE = "transfer to";

        public static string lastGif = "";
        public static string currentGif = "";

        public async Task StartAsync(IDialogContext context)
        {

            /* Wait until the first message is received from the conversation and call MessageReceviedAsync 
            *  to process that message. */
            context.Wait(this.MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            /* When MessageReceivedAsync is called, it's passed an IAwaitable<IMessageActivity>. To get the message,
             *  await the result. */
            var message = await result;

            List<string> Gifs = new List<string>
            {              
                "https://media.giphy.com/media/15ZIAThLx0U92/giphy.gif",
                "https://media.giphy.com/media/13HZaEntj8P22Q/giphy.gif",
                "https://media.giphy.com/media/3o7btT1T9qpQZWhNlK/giphy.gif",
                "https://media.giphy.com/media/B9DflEzGDqsms/giphy.gif"
            };

            if (message.Text.ToLower() == "show me a gif")
            {
                Activity replyToConversation = (Activity)context.MakeMessage();
                var connector = new ConnectorClient(new Uri(replyToConversation.ServiceUrl));
                replyToConversation.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                replyToConversation.Attachments = new List<Attachment>();


                while (true)
                {
                    Random rnd = new Random();
                    var i = rnd.Next(0, Gifs.Count);
                    if (lastGif != Gifs[i] || lastGif == "")
                    {
                        currentGif = Gifs[i];
                        lastGif = Gifs[i];
                        break;
                    }
                }

                List<CardImage> cardImages = new List<CardImage>();
                cardImages.Add(new CardImage(url: currentGif));

                List<CardAction> cardButtons = new List<CardAction>();
                
                HeroCard plCard = new HeroCard()
                {
                    Title = $"Example Title",
                    Subtitle = $"Example Subtitle",
                    Images = cardImages
                };

                Attachment plAttachment = plCard.ToAttachment();
                replyToConversation.Attachments.Add(plAttachment);

                var reply = await connector.Conversations.SendToConversationAsync(replyToConversation);
            }
            
            else if (message.ChannelId == ChannelIds.Directline)
            {
                var laChannelData = message.GetChannelData<LiveAssistChannelData>();

                if (laChannelData.Type == "visitorMessage")
                {
                    if (message.Text.StartsWith(TRANSFER_MESSAGE))
                    {
                        var reply = context.MakeMessage();
                        var transferTo = message.Text.Substring((TRANSFER_MESSAGE).Length);

                        reply.ChannelData = new LiveAssistChannelData()
                        {
                            Type = "transfer",
                            Skill = transferTo
                        };

                        await context.PostAsync(reply);
                    }
                }
            }
            else
            {
                var qnaAuthKey = ConfigurationManager.AppSettings[QNA_AUTH_KEY_CONFIGURATION_KEY_NAME];
                var qnaKBId = ConfigurationManager.AppSettings[QNA_KNOWLEDGE_BASE_CONFIGURATION_KEY_NAME];
                var endpointHostName = ConfigurationManager.AppSettings[QNA_ENDPOINT_HOST_NAME_CONFIGURATION_KEY_NAME];


                // QnA Subscription Key and KnowledgeBase Id null verification
                if (!string.IsNullOrEmpty(qnaAuthKey) && !string.IsNullOrEmpty(qnaKBId))
                {
                    string googleSearch = GenerateGoogleSearchUrl(message.Text);

                    // Forward to the appropriate Dialog based on whether the endpoint hostname is present
                    if (string.IsNullOrEmpty(endpointHostName))
                        await context.Forward(new BasicQnAMakerPreviewDialog(googleSearch), AfterAnswerAsync, message, CancellationToken.None);
                    else
                        await context.Forward(new BasicQnAMakerDialog(googleSearch), AfterAnswerAsync, message, CancellationToken.None);
                }
                else
                {
                    await context.PostAsync("Please set QnAKnowledgebaseId, QnAAuthKey and QnAEndpointHostName (if applicable) in App Settings. Learn how to get them at https://aka.ms/qnaabssetup.");
                }
            }
        }

        private async Task AfterAnswerAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            // wait for the next user message
            context.Wait(MessageReceivedAsync);
        }


        [Obsolete("Use ConfigurationManager.AppSettings[<<key>>] instead", true)]
        private static string GetSetting(string key)
        {
            var value = Utils.GetAppSetting(key);
            if (String.IsNullOrEmpty(value) && key == "QnAAuthKey")
            {
                value = Utils.GetAppSetting("QnASubscriptionKey"); // QnASubscriptionKey for backward compatibility with QnAMaker (Preview)
            }
            return value;
        }

        private static string GenerateGoogleSearchUrl(string messageText)
        {
            // Escape the query.
            string httpEscapedText = System.Security.SecurityElement.Escape(messageText);

            // Replace the %20 with a space so we can use String.Split().
            string searchParam = string.Join("+", httpEscapedText.Replace("%20", " ").Split(' '));

            string googleSearch = $"I don't know.. Let me google that for you!\nhttps://www.google.com/search?q={ searchParam }";

            return messageText;
        }
    }

}