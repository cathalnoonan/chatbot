﻿using Microsoft.Bot.Builder.CognitiveServices.QnAMaker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace QnABot.Dialogs
{
    // Dialog for QnAMaker GA service
    [Serializable]
    public class BasicQnAMakerDialog : QnaMakerDialogAbstract
    {        
        // Go to https://qnamaker.ai and feed data, train & publish your QnA Knowledgebase.
        // Parameters to QnAMakerService are:
        // Required: qnaAuthKey, knowledgebaseId, endpointHostName
        // Optional: defaultMessage, scoreThreshold[Range 0.0 – 1.0]
        public BasicQnAMakerDialog(string message = "No good match in FAQ.")
            : base(new QnAMakerService(new QnAMakerAttribute(qnaAuthKey, qnaKBId, message, 0.5, 1, endpointHostName)))
        { }

    }
}