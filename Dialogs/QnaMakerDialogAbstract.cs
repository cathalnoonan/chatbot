﻿using Microsoft.Bot.Builder.CognitiveServices.QnAMaker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace QnABot.Dialogs
{
    [Serializable]
    public abstract class QnaMakerDialogAbstract : QnAMakerDialog
    {
        protected internal static readonly string qnaAuthKey = ConfigurationManager.AppSettings["QnAAuthKey"];
        protected internal static readonly string qnaKBId = ConfigurationManager.AppSettings["QnAKnowledgebaseId"];
        protected internal static readonly string endpointHostName = ConfigurationManager.AppSettings["QnAEndpointHostName"];

        public QnaMakerDialogAbstract(params IQnAService[] services) : base(services) { }
    }
}