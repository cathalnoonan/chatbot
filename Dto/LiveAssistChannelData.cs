﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QnABot.Dto
{
    // Live Assist custom channel data.
    public class LiveAssistChannelData
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("skill", NullValueHandling = NullValueHandling.Ignore)]
        public string Skill { get; set; }
    }
}